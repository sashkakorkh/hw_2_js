/*Exercise 1*/
/*1.Типи даних в JS:
* String- символи та літери
* Number - числа 64 бітного floating point формату
* BigInt - числа більше ніж +2^53
* Boolean - булінові значення true/ false
* Underfined -  для неуіснуючого значення
* Null - для визначення відсутності будь-якого значення
* Symbol - унікальні ідентифікатори
* Object* - сукупність пов'язаних даних/

* 2. У чому різниця між == і ===?
* Це оператори порівняння. Оператор == виконує не строге порівняння,
* під час якого може відбутись неявне приведення типів, тому його не рекомендовано використовувати для перевірок.
* не відрізняє 0 від false.
* Оператор === виконує строге порівння, без приведення типів. NaN не дорівнює NaN.

 * 3. Оператор - це спеціальні символи які мають своє функціональне призначення.
 *  Вони виконують  дії над операндами(унарний оператор над одним оператором, бінарний над двома, тернарний - 3).
 *  За призначенням поділяються на логічні оператори, оператори порівняння, оператор присвоєння,
 * оператор поєднання з null та умовний оператор */

/*Exercise 2*/
let userName = prompt('What\'s your name?');
let userAge = prompt('What\'s your age?');

while (!userName || !isNaN(userName) || userName === 'null' ) {
    userName = prompt('What\'s your name?', userName);
}

while (!userAge || isNaN(userAge) || userName === 'null') {
    userAge = prompt('What\'s your age?', userAge)
}


if (+userAge > 22) {
    alert(`Welcome ` + `${userName}`);
} else if (+userAge > 17 && +userAge < 23) {
    let userAnswer = confirm('Are you sure you want to continue?');
    if (userAnswer === true) {
        alert(`Welcome ` + `${userName}`);
    } else {
        alert('You are not allowed to visit this website.');
    }
}else{
    alert('You are not allowed to visit this website.');
}

